import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
//import QtQuick.Controls 6

import Schedule 1.0

ApplicationWindow {
    id: root
    width: 640
    height: 480
    visible: true
    title: qsTr("Bus schedule")

    menuBar: MenuBar {
        Menu {
            title: qsTr("&File")
            Action {
                text: qsTr("&Update schedule")
                onTriggered: {
                    _view.updateRoutes()
                }
            }
        }
    }

    GridLayout {
        id: _layout
        anchors.fill: parent

        rows: 3
        columns: 2

        ComboBox {
            id: _comboboxRoute
            model: _view.routes()
            displayText: qsTr("Route: ") + currentText

            Layout.fillWidth: true
            Layout.row: 0
            Layout.column: 0

            onCurrentTextChanged: {
                _view.setModel(currentText, _comboboxStop.currentText, _switchDirection.checked)
            }
        }

        ComboBox {
            id: _comboboxStop
            displayText: qsTr("Stop: ") + currentText
            model: _view.stops()

            Layout.fillWidth: true
            Layout.row: 0
            Layout.column: 1

            onCurrentTextChanged: {
                _view.setModel(_comboboxRoute.currentText, currentText, _switchDirection.checked)
            }
        }

        Switch {
            id: _switchDirection

            Layout.fillWidth: true
            Layout.row: 1
            Layout.column: 0

            Layout.columnSpan: _layout.columns

            checked: true
            text: (checked) ? qsTr("From Herzen") : qsTr("To Herzen");

            onClicked: {
                _view.setModel(_comboboxRoute.currentText, _comboboxStop.currentText, checked)
            }
        }

        StopScheduleView {
            id: _view

            Layout.row: 2
            Layout.column: 0

            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.columnSpan: _layout.columns

            onRoutesDataChanged: {
                _comboboxRoute.model = _view.routes()
                _comboboxStop.model = _view.stops()
            }
        }

    }

}
