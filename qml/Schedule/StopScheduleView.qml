import QtQuick 2.0
import QtQuick.Layouts 1.15

import Schedule 1.0

GridLayout {
    id: _root

    rows: 1
    columns: 1

    signal routesDataChanged

    ScheduleView {
        id: _view

        Layout.fillHeight: true
        Layout.fillWidth: true
        Layout.row: 0
        Layout.column: 0

        delegate: Item {
            id: _backgroundDelegate
            width: root.width * 1.0
            height: root.height * 0.2

            Tile {
                anchors.fill: _backgroundDelegate
                anchors.margins: 5

                internalText.text: text.toString()
                color: {
                    return model.color
                }
            }
        }
    }

    ScheduleModelController {
        id: _controller
    }

    Connections {
        target: _controller
        function onDataChanged() {
            _root.routesDataChanged()
        }
    }

    function setModel(route: string, stop: string, direction: bool) {
        _view.model = _controller.getModel(route, stop, direction)
    }

    function updateRoutes() {
        _controller.fetchRoutesFromGitLab()
    }

    function routes() {
        return _controller.routes()
    }

    function stops() {
        return _controller.stops()
    }
}
