import QtQuick 2.0

Rectangle {
    id: root

    property alias internalText: valueText

    color: "lightgreen"

    Text {
        id: valueText

        font.pointSize: 15

        anchors.centerIn: root
    }
}
