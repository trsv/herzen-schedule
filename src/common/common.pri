QT += core

INCLUDEPATH += $$PWD

HEADERS += \
    $$PWD/io/fileio.h \
    $$PWD/io/jsonio.h

SOURCES += \
    $$PWD/io/fileio.cpp \
    $$PWD/io/jsonio.cpp

