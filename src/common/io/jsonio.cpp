#include "jsonio.h"

void JsonIO::write(const QString &path, const QJsonArray &array) const
{
    FileIO::write(path, QJsonDocument(array).toJson(QJsonDocument::Compact));
}

QJsonArray JsonIO::readArray(const QString &path) const
{
    return QJsonDocument::fromJson(FileIO::read(path)).array();
}

void JsonIO::writeObject(const QString &path, const QJsonObject &json) const
{
    FileIO::write(path, QJsonDocument(json).toJson(QJsonDocument::Compact));
}

QJsonObject JsonIO::readObject(const QString &path) const
{
    return QJsonDocument::fromJson(FileIO::read(path)).object();
}
