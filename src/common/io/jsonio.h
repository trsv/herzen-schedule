#ifndef JSONIO_H
#define JSONIO_H

#include "fileio.h"

#include <QJsonDocument>

class JsonIO : private FileIO
{
public:
    void write(const QString & path, const QJsonArray & array) const;
    QJsonArray readArray(const QString &path) const;

    void writeObject(const QString & path, const QJsonObject & json) const;
    QJsonObject readObject(const QString &path) const;
};

#endif // JSONIO_H
