#ifndef FILEIO_H
#define FILEIO_H

#include <QtCore>

class FileIO
{
public:
    FileIO();

    QByteArray read(const QString & path) const;
    void write(const QString & path, const QByteArray & data, bool isAppend = false) const;
};

#endif // FILEIO_H
