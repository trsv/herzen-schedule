#include "fileio.h"

FileIO::FileIO()
{

}

QByteArray FileIO::read(const QString &path) const
{
    QFile file(path);
    return (file.open(QIODevice::ReadOnly)) ? file.readAll() : QByteArray();
}

void FileIO::write(const QString &path, const QByteArray &data, bool isAppend) const
{
    QFile file(path);
    if (!QDir().mkpath(QFileInfo(file).absolutePath())) {
        return;
    }

    const auto isOpen = (isAppend) ? file.open(QIODevice::WriteOnly | QIODevice::Append) :
                                     file.open(QIODevice::WriteOnly);
    if (isOpen) {
        file.write(data);
    }
}
