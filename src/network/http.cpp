#include "http.h"

const QNetworkAccessManager &Http::manager() const
{
    return m_manager;
}

Http::Http(QObject *parent)
    : QObject(parent)
{
    m_manager.setTransferTimeout(10 * 1000);

    connect(&m_manager, &QNetworkAccessManager::finished,
            this, &Http::onReplyFinished);
}

QNetworkReply *Http::sendRequest(const QByteArray &method, const QNetworkRequest &request,
                       const QByteArray &data)
{
    return m_manager.sendCustomRequest(request, method, data);
}

void Http::onReplyFinished(QNetworkReply *reply)
{
    reply->deleteLater();
}
