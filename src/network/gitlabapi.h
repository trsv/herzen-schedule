#ifndef GITLABAPI_H
#define GITLABAPI_H

#include "http.h"

class GitLabApi : public Http
{
    Q_OBJECT

    using FilePathNameMap = QMap <QString, QString>;

    void onRoutesFetched(QNetworkReply * reply);

public:
    explicit GitLabApi(QObject * parent);

    void fetchRoutes();
    void fetchFileRaw(const QString & path);
    void fetchFilesRaw(const FilePathNameMap & files);

signals:
    void routesFetched(const FilePathNameMap & routes) const;
    void fileRawFetched(const QString & path, const QByteArray & raw) const;
};

#endif // GITLABAPI_H
