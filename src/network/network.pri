QT += network

INCLUDEPATH += $$PWD

HEADERS += \
    $$PWD/gitlabapi.h \
    $$PWD/http.h

SOURCES += \
    $$PWD/gitlabapi.cpp \
    $$PWD/http.cpp
