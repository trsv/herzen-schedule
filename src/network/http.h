#ifndef HTTP_H
#define HTTP_H

#include <QtNetwork>

class Http : public QObject
{
    Q_OBJECT

    QNetworkAccessManager m_manager;

public:
    explicit Http(QObject * parent);

    const QNetworkAccessManager &manager() const;

    QNetworkReply * sendRequest(const QByteArray & method, const QNetworkRequest & request,
                     const QByteArray & data = {});
    virtual void onReplyFinished(QNetworkReply * reply);
};

#endif // HTTP_H
