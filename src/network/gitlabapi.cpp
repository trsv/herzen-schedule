#include "gitlabapi.h"

void GitLabApi::onRoutesFetched(QNetworkReply *reply)
{
    const auto json = QJsonDocument::fromJson(reply->readAll()).array();
    reply->deleteLater();

    FilePathNameMap map;
    for (const auto & raw : json) {
        const auto obj = raw.toObject();
        if (obj["type"].toString() == "blob") {
            map[obj["path"].toString()] = obj["name"].toString();
        }
    }

    emit routesFetched(map);
}

GitLabApi::GitLabApi(QObject *parent)
    : Http(parent)
{

}

void GitLabApi::fetchRoutes()
{
    QUrlQuery query;
    query.addQueryItem("path", "src/rsc/route");
    query.addQueryItem("recursive", "true");

    QUrl url("https://gitlab.com/api/v4/projects/24381780/repository/tree");
    url.setQuery(query);

    auto reply = sendRequest("GET", QNetworkRequest(url));
    connect(reply, &QNetworkReply::finished,
            this, [this, reply]
    {
        onRoutesFetched(reply);
    });
}

void GitLabApi::fetchFileRaw(const QString &path)
{
    const auto endpoint = QString("https://gitlab.com/api/v4/projects/24381780/repository/files/%1/raw")
            .arg(QString(QUrl::toPercentEncoding(path)));
    auto reply = sendRequest("GET", QNetworkRequest(QUrl(endpoint)));
    connect(reply, &QNetworkReply::finished,
            this, [this, reply, path]
    {
        emit fileRawFetched(path, reply->readAll());
        reply->deleteLater();
    });
}

void GitLabApi::fetchFilesRaw(const FilePathNameMap &files)
{
    for (auto it = files.begin(); it != files.end(); ++it) {
        fetchFileRaw(it.key());
    }
}
