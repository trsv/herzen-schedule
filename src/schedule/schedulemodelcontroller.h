#ifndef SCHEDULEMODELCONTROLLER_H
#define SCHEDULEMODELCONTROLLER_H

#include "scheduleitemmodel.h"
#include "network/gitlabapi.h"

class ScheduleModelController : public QObject
{
    Q_OBJECT

    using StopList = QMap <QString, ScheduleItemModel*>;
    using RouteList = QMap <QString, StopList>;

    RouteList m_routesTo;
    RouteList m_routesFrom;

    GitLabApi m_gitlab;

    /**
     * @brief restoreRoutes - read all route schedules from files in resources
     */
    void restoreRoutes();


    /**
     * @brief restoreRoutes - read routes from given @path and save to @routes
     * @param path
     * @param routes
     */
    void restoreRoutes(const QString & path, RouteList & routes);


    void dumpRoutes() const;


    void dumpRout(const RouteList & route) const;

public:
    ScheduleModelController();

    void setModel(ScheduleItemModel *model, bool isFrom);
    Q_INVOKABLE ScheduleItemModel * getModel(const QString & route, const QString & stop,
                                             bool isFrom) const;
    Q_INVOKABLE void fetchRoutesFromGitLab();

    Q_INVOKABLE QStringList routes() const;
    Q_INVOKABLE QStringList stops() const;

signals:
    void dataChanged() const;
};

#endif // SCHEDULEMODELCONTROLLER_H
