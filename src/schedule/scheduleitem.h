#ifndef SCHEDULEITEM_H
#define SCHEDULEITEM_H

#include <QTime>

class ScheduleItem
{
    QTime m_time;
    QString m_timeStr;

    void setTimeStr(const QString &timeStr);

public:
    ScheduleItem();

    QString timeStr() const;

    void setTime(const QString & time);
    void setTime(const QTime & time);

    bool isMissed() const;

    bool operator < (const ScheduleItem & other) const;
};

#endif // SCHEDULEITEM_H
