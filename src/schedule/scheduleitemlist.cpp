#include "scheduleitemlist.h"

void ScheduleItemList::sort()
{
    std::sort(m_list.begin(), m_list.end());
}

QJsonArray ScheduleItemList::toJson() const
{
    QJsonArray json;
    for (const auto & item : m_list) {
        json << item.timeStr();
    }

    return json;
}

ScheduleItemList::ScheduleItemList()
{

}

void ScheduleItemList::append(const ScheduleItem &item)
{
    insert(size(), item);
}

void ScheduleItemList::insert(int index, const ScheduleItem &item)
{
    m_list.insert(index, item);
}

void ScheduleItemList::removeAt(int index)
{
    if (index >= 0 && index < size()) {
        m_list.removeAt(index);
    }
}

const ScheduleItem &ScheduleItemList::at(int index) const
{
    return m_list.at(index);
}

ScheduleItem &ScheduleItemList::at(int index)
{
    return m_list[index];
}

int ScheduleItemList::size() const
{
    return m_list.size();
}
