#include "schedulemodelcontroller.h"

#include <QDirIterator>
#include <QDebug>

void ScheduleModelController::restoreRoutes()
{
    restoreRoutes("./resource/route/to", m_routesTo);
    restoreRoutes("./resource/route/from", m_routesFrom);
}

void ScheduleModelController::restoreRoutes(const QString &path, RouteList &routes)
{
    QDirIterator it(path, QStringList() << "*.json", QDir::Filter::Files,
                    QDirIterator::Subdirectories);

    while (it.hasNext()) {
        auto model = new ScheduleItemModel(it.next(), this);
        routes[model->route()][model->stop()] = model;
    }
}

void ScheduleModelController::dumpRoutes() const
{
    dumpRout(m_routesFrom);
    dumpRout(m_routesTo);
}

void ScheduleModelController::dumpRout(const RouteList &route) const
{
    for (auto it = route.begin(); it != route.end(); ++it) {
        for (const auto & stop : it.value()) {
            stop->dump();
        }
    }
}

ScheduleModelController::ScheduleModelController()
    : m_gitlab(this)
{
     restoreRoutes();

     connect(&m_gitlab, &GitLabApi::routesFetched,
             &m_gitlab, &GitLabApi::fetchFilesRaw);

     connect(&m_gitlab, &GitLabApi::fileRawFetched,
             this, [this] (const QString & path, const auto & raw)
     {
         setModel(new ScheduleItemModel(path, QJsonDocument::fromJson(raw).array(), this),
                  path.contains("/from/"));

         emit dataChanged();
     });
}

void ScheduleModelController::setModel(ScheduleItemModel *model, bool isFrom)
{
    if (auto model2 = getModel(model->route(), model->stop(), isFrom)) {
        model2->deleteLater();
    }

    auto & routes = (isFrom) ? m_routesFrom : m_routesTo;
    routes[model->route()][model->stop()] = model;

    dumpRoutes();
}

ScheduleItemModel *ScheduleModelController::getModel(const QString &route,
                                                     const QString &stop, bool isFrom) const
{
    const auto & routes = (isFrom) ? m_routesFrom : m_routesTo;
    return routes[route].value(stop, nullptr);
}

void ScheduleModelController::fetchRoutesFromGitLab()
{
    m_gitlab.fetchRoutes();
}

QStringList ScheduleModelController::routes() const
{
    QStringList routes;
    routes << m_routesFrom.keys();
    routes << m_routesTo.keys();
    routes.removeDuplicates();
    return routes;
}

QStringList ScheduleModelController::stops() const
{
    QStringList stops;

    for (auto it = m_routesFrom.begin(); it != m_routesFrom.end(); ++it) {
        stops << it.value().keys();
    }

    for (auto it = m_routesTo.begin(); it != m_routesTo.end(); ++it) {
        stops << it.value().keys();
    }

    stops.removeDuplicates();
    return stops;
}
