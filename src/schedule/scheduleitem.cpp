#include "scheduleitem.h"

void ScheduleItem::setTimeStr(const QString &timeStr)
{
    m_timeStr = timeStr;
}

ScheduleItem::ScheduleItem()
{
    setTime(QTime::currentTime());
}

QString ScheduleItem::timeStr() const
{
    return m_timeStr;
}

void ScheduleItem::setTime(const QString &time)
{
    m_time = QTime::fromString(time);
    setTimeStr(time);
}

void ScheduleItem::setTime(const QTime &time)
{
    m_time = time;
    setTimeStr(time.toString("HH:mm"));
}

bool ScheduleItem::isMissed() const
{
    return m_time < QTime::currentTime();
}

bool ScheduleItem::operator <(const ScheduleItem &other) const
{
    return m_time < other.m_time;
}
