#ifndef SCHEDULEITEMLIST_H
#define SCHEDULEITEMLIST_H

#include "scheduleitem.h"

#include <QJsonArray>

class ScheduleItemList
{
    QList <ScheduleItem> m_list;

public:
    ScheduleItemList();

    void append(const ScheduleItem & item);
    void insert(int index, const ScheduleItem & item);
    void removeAt(int index);
    const ScheduleItem & at(int index) const;
    ScheduleItem & at(int index);
    int size() const;
    void sort();

    QJsonArray toJson() const;
};

#endif // SCHEDULEITEMLIST_H
