#ifndef SCHEDULEITEMMODEL_H
#define SCHEDULEITEMMODEL_H

#include "scheduleitemlist.h"

#include <QAbstractListModel>

class ScheduleItemModel : public QAbstractListModel
{
    Q_OBJECT

    bool m_isFrom;

    // route number
    QString m_route;

    // stop name
    QString m_stop;

    // list of times
    ScheduleItemList m_items;

    bool isIndexValid(int index) const;
    void setStop(QString stop);
    void restore(const QString & path);
    void restore(const QString & path, const QJsonArray & json);

public:
    ScheduleItemModel(QObject * parent = nullptr);
    ScheduleItemModel(const QString & path, QObject * parent = nullptr);
    ScheduleItemModel(const QString & path, const QJsonArray & json, QObject * parent = nullptr);

    int rowCount(const QModelIndex &parent = {}) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    bool insertRows(int position, int rows, const QModelIndex &parent = {}) override;
    bool removeRows(int position, int rows, const QModelIndex &parent = {}) override;
    QHash<int, QByteArray> roleNames() const override;

    Q_INVOKABLE void add();
    Q_INVOKABLE void add(const QString & time);

    QString stop() const;
    QString route() const;

    void dump() const;
};

#endif // SCHEDULEITEMMODEL_H
