#include "scheduleitemmodel.h"
#include "common/io/jsonio.h"

#include <QDebug>
#include <QColor>

QString ScheduleItemModel::stop() const
{
    return m_stop;
}

QString ScheduleItemModel::route() const
{
    return m_route;
}

void ScheduleItemModel::dump() const
{
    const auto direction = (m_isFrom) ? "from" : "to";
    const auto path = QString("./resource/route/%1/%2/%3.json")
            .arg(direction, route(), stop());
    JsonIO().write(path, m_items.toJson());
}

bool ScheduleItemModel::isIndexValid(int index) const
{
    return index < m_items.size();
}

void ScheduleItemModel::setStop(QString stop)
{
    if (stop.endsWith(".json")) {
        stop.remove(".json");
    }

    m_stop = stop;
}

void ScheduleItemModel::restore(const QString &path)
{
    restore(path, JsonIO().readArray(path));
}

void ScheduleItemModel::restore(const QString &path, const QJsonArray &json)
{
    m_isFrom = path.contains("/from/");

    for (const auto & itemJson : json) {
        // add times
        add(itemJson.toString());
    }

    m_items.sort();

    // set route and stop name
    if (const auto pathData = path.split("/"); pathData.size() > 1) {
        setStop(pathData.last());
        m_route = pathData[pathData.size() - 2];
    }
}

ScheduleItemModel::ScheduleItemModel(QObject *parent)
    : QAbstractListModel(parent)
    , m_isFrom(false)
{

}

ScheduleItemModel::ScheduleItemModel(const QString &path, QObject *parent)
    : ScheduleItemModel(parent)
{
    restore(path);
}

ScheduleItemModel::ScheduleItemModel(const QString &path, const QJsonArray &json, QObject *parent)
    : ScheduleItemModel(parent)
{
    restore(path, json);
}

int ScheduleItemModel::rowCount(const QModelIndex &/*parent*/) const
{
    return m_items.size();
}

QVariant ScheduleItemModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || !isIndexValid(index.row())) {
        return {};
    }

    switch (role) {
    case Qt::DisplayRole: return m_items.at(index.row()).timeStr();
    case Qt::BackgroundRole: return (m_items.at(index.row()).isMissed()) ?
                    QColor(255, 204, 204) : QColor(204, 255, 204);
    default: return {};
    }
}

bool ScheduleItemModel::insertRows(int position, int rows, const QModelIndex &parent)
{
    beginInsertRows(parent, position, position + rows - 1);

    for (int row = 0; row < rows; row++) {
        m_items.insert(position, ScheduleItem());
    }

    endInsertRows();
    return true;
}

bool ScheduleItemModel::removeRows(int position, int rows, const QModelIndex &parent)
{
    if (rows) {
        beginRemoveRows(parent, position, position + rows - 1);

        for (int row = 0; row < rows; row++) {
            m_items.removeAt(position);
        }

        endRemoveRows();
    }

    return true;
}

void ScheduleItemModel::add()
{
    insertRow(0);
}

void ScheduleItemModel::add(const QString &time)
{
    insertRow(0);
    m_items.at(0).setTime(time);
}


QHash<int, QByteArray> ScheduleItemModel::roleNames() const
{
    return {
        {Qt::DisplayRole, "text"},
        {Qt::BackgroundRole, "color"}
    };
}
