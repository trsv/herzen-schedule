QT += core

INCLUDEPATH += $$PWD

SOURCES +=  \
    $$PWD/scheduleitem.cpp \
    $$PWD/scheduleitemlist.cpp \
    $$PWD/scheduleitemmodel.cpp \
    $$PWD/schedulemodelcontroller.cpp

HEADERS +=  \
    $$PWD/scheduleitem.h \
    $$PWD/scheduleitemlist.h \
    $$PWD/scheduleitemmodel.h \
    $$PWD/schedulemodelcontroller.h

